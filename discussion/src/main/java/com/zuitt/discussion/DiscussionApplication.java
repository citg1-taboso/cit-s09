package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@SpringBootApplication
//This application will function as an endpoint that we will use in handling http requests
@RestController
public class DiscussionApplication {
//	RUN the App: ./mvnw spring-boot:run

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	// 	Getting a get request
	@GetMapping("/hello")
//	Maps a get request to the route /hello and invokes the method hello()
	public String hello() {
		return "Hello World!";
	}

	//	Route with String query
	@GetMapping("/hi")
	public String hi(@RequestParam(value = "name", defaultValue = "John") String name) {
		return "Hello " + name;
	}

	//	Multiple parameters
//	localhost:8080/friend?name=value&friend=value
	@GetMapping("/friend")
	public String friend(@RequestParam(value = "name", defaultValue = "Joe") String name, @RequestParam(value = "friend", defaultValue = "Mason") String friend) {
		return String.format("Hello %s, My name is %s", friend, name);
	}

	//	Route with path variables
//	Dynamic data is obtained directly from the url
	@GetMapping("/hello/{name}")
//	Path variable allows us to extract data directly from the URL
	public String greetFriend(@PathVariable("name") String name) {
		return String.format("Nice to meet you %s!", name);
	}

	//	Activity S09 - Synchronous Activity
	ArrayList<Student> students = new ArrayList<Student>();

	@GetMapping("welcome/")
	public String greetRole(@RequestParam(value = "user") String user, @RequestParam(value = "role") String role) {
		if (role.equals("admin")) {
			return String.format("Welcome back to the class portal, Admin %s!", user);
		} else if (role.equals("teacher")) {
			return String.format("Thank you for logging in, Teacher %s!", user);
		} else if (role.equals("student")) {
			return String.format("Welcome to the class portal, %s!", user);
		} else {
			return "Role out of range!";
		}
	}

	@GetMapping("/register")
	public String registerStudent(@RequestParam(value = "id") String id, @RequestParam(value = "name") String name, @RequestParam("course") String course) {
		Student student = new Student(id, name, course);
		students.add(student);

		return String.format("%s your id number is register on the system!", id);
	}

	@GetMapping("/account/{id}")
	public String findAccount(@PathVariable("id") String id) {
		for (int i = 0; i < students.size(); i++) {
			if (id.equals(students.get(i).id)) {
				return String.format("Welcome back %s! You are currently enrolled in %s!", students.get(i).name, students.get(i).course);
			}
		}
		return String.format("Your provided %s is not found in the system!", id);
	}
	// S09 - Assignment Activity
	ArrayList<String> enrollees = new ArrayList<String>();

//	Enrolling users to the ArrayList
	@GetMapping("/enroll")
	public String enroll(@RequestParam(value = "user") String user) {
		enrollees.add(user);
		return String.format("Thank you for enrolling, %s!", user);
	}

//	Getting enrollees
	@GetMapping("/getEnrollees")
	public String getEnrolless() {
		return String.format(String.valueOf(enrollees));
	}

	//name and age mapping
	@GetMapping("/nameage")
	public String nameAge(@RequestParam(value="name") String name, @RequestParam(value = "age") String age) {
		return String.format("Hello %s! My age is %s.", name, age);
	}

	//courses and ID
	@GetMapping("/courses/{id}")
	public String courses(@PathVariable("id")String id) {
		if(id.equals("java101")) {
			return "Java 101, MWF 8:00 AM-11:00 AM, PHP 3000.00";
		} else if(id.equals("sql101")) {
			return "SQL 101, TTH 1:00 PM-4:00 PM, PHP 2000.00";
		} else if(id.equals("javaee101")) {
			return "JAVA EE 101, MWF 1:00 PM-4:00 PM, PHP 3500.00";
		} else {
			return "Course cannot be found!";
		}
	}

}

class Student {
	String id;
	String name;
	String course;

	public Student(){ }

	public Student(String id, String name, String course) {
		this.id = id;
		this.name = name;
		this.course = course;
	}




}



